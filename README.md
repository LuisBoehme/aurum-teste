## 🚀 Quick start

1.  **Prerequisites to develop this site**
    
    -Node.js intalled
    
    -A Package manager, such as npm or yarn
    
    -Git installed

2.  **Clone the repository.**

    Navigate into the folder you want to clone the repository and enter this command:

    ```git clone https://LuisBoehme@bitbucket.org/*YOURGITUSER*/aurum-teste.git```

    Then all you have to do is run `npm install` or `yarn` and wait for the packages to be installed

3.  **Open the source code and start editing!**

4.  **View your changes live**

    While coding you should run the `gatsby develop` command. This will generate a localhos server so you can view your changes while you code.

    Your site should be running at `http://localhost:8000`!

    _Note: You'll also see a second link: _`http://localhost:8000/___graphql`_. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)._

## 🧐 What's inside?

A quick look at the top-level files and directories you'll see in a Gatsby project.

    .
    ├── node_modules
    ├─ src
    | ├── components
    | └── pages
    ├── .gitignore
    ├── .prettierrc
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    └── README.md

1.  **`/node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages) are automatically installed.

2.  **`/src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. `src` is a convention for “source code”.

  2.1  **`/src/components`**: Everything you code should be implemented as a react component and put in a folder inside this one.

  2.2  **`/src/pages`**: This is where your page files will be, but remember not every route should have its own page, you should create templates for the routes.

3.  **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.

4.  **`.prettierrc`**: This is a configuration file for [Prettier](https://prettier.io/). Prettier is a tool to help keep the formatting of your code consistent.

5.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more detail).

6.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.

7.  **`LICENSE`**: Gatsby is licensed under the MIT license.

8. **`package-lock.json`** (See `package.json` below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(You won’t change this file directly).**

9. **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.

10. **`README.md`**: A text file containing useful reference information about your project.

## 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.org/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

## 💫 Deploy

To deploy a build, all you have to do is submit a merge request to the master branch in this repository, and Netlify will handle everything for you, and your changes will be made avaliable in a few minutes in the following address:

```private.luisboehme.com```

##  THAT'S ALL, HAPPY CODING!

Att, Luis Boehme.
