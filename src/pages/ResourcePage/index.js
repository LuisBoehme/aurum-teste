import React from "react"

const ResourcePage = ({ data }) => {
  console.log(data)
  return(
      <h1>Blog Post Template</h1>
  )
}

export default ResourcePage
export const query = graphql`
  query {
    allWordpressAcfPages {
      edges {
        node {
          id
          acf {
            title
            description
            category
            image {
              id
            }
          }
        }
      }
    }
  }
`
