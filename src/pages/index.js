import React from "react"
import { useStaticQuery, graphql } from "gatsby"

import SEO from "../components/seo"
import PostItem from "../components/Posts/PostItem"
import Filter from "../components/Filter"

import './styles.css'

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFilter: 'Todos'
    }
    this.handleFilter= this.handleFilter.bind(this)
  }

  handleFilter(selected) {
    this.setState({ selectedFilter: selected })
  }

  data() {
    useStaticQuery(graphql`
    query ResourcePagesQuery {
        allWordpressAcfPages {
          edges {
            node {
              id
              acf {
                title
                description
                category
                image {
                  id
                  localFile {
                    childImageSharp {
                      fixed {
                        ...GatsbyImageSharpFixed
                      }
                    }
                  }
                }
              }
            }
          }
        }
    }
  `)
  }


  render(){
  console.log(this.state.selectedFilter)
  const pagesEdges  = this.props.data.allWordpressAcfPages.edges
  return(
    <>
      <Filter selectedFilter={this.state.selectedFilter} onSelectFilter={this.handleFilter}/>
      <main>
      <SEO title="Home" />
        {
          pagesEdges.map(edge => {
            const post = edge.node.acf
            console.log(post.category)
            return(
              <PostItem shouldShow={this.state.selectedFilter === 'Todos' ||  this.state.selectedFilter === post.category }  key={edge.node.id} title={post.title} description={post.description} category={post.category} img={post.image.localFile.childImageSharp.fixed}/>
            )
          })
        }
      </main>
    </>
  )
}
}

export default MainPage
