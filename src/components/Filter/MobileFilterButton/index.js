import React from "react"
import './styles.css';
import AllIcon from '../../../icons/stack.svg';

const MobileFilterButton = ({onClick}) =>  (
  <div onClick={onClick} className='mobile-filter-button-wrapper'>
    <AllIcon />
      <span className='mobile-filter-description'>Filtros</span>
  </div>
)

export default MobileFilterButton
