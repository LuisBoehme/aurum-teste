import React from "react"
import './styles.css';

class FilterItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFilter: 'Todos'
    }
  }
  render(){
    return(
      <div onClick={this.props.onClick} className={`category-item ${this.props.isActive ? 'active' : ''}`}>
        {this.props.icon}
        <span className='category-name'>{this.props.name}</span>
      </div>
    )
  }
}

export default FilterItem
