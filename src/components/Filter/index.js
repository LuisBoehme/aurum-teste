import React from "react"
import { Container, Row, Col } from 'react-grid-system';
import FilterItem from './FilterItem';
import MobileFilterButton from './MobileFilterButton';

import AllIcon from '../../icons/stack.svg';
import EbookIcon from '../../icons/book.svg';
import InfogIcon from '../../icons/chart.svg';
import WebinarIcon from '../../icons/play.svg';
import ToolIcon from '../../icons/gear.svg';
import './styles.css';

class Filter extends React.Component {
constructor(props) {
    super(props)
    this.handleFilterChange = this.handleFilterChange.bind(this)
    this.showFilterMenu = this.showFilterMenu.bind(this)
}
  state = {
    selectedFilter: 'Todos',
    mobileFilterVisible: false,
  }

  handleFilterChange(filter) {
    this.props.onSelectFilter(filter)           
    this.setState({selectedFilter: filter, mobileFilterVisible: false})
  }

  showFilterMenu(){
    this.setState({ mobileFilterVisible: true})
  }

  render() {
    return(
      <>
      <MobileFilterButton onClick={this.showFilterMenu}/>
        <div className={`header-wrapper ${this.state.mobileFilterVisible && 'mobile-visible'}`}> 
          <Container>
            <Row>
              <Col>
                <div className='filter-wrapper'> 
                  <FilterItem onClick={() => this.handleFilterChange('Todos')} name="Todos" icon={<AllIcon />} isActive={this.state.selectedFilter === 'Todos'}/>
                  <FilterItem onClick={() => this.handleFilterChange('E-Books')} name="E-Books" icon={<EbookIcon />}  isActive={this.state.selectedFilter === 'E-Books'}/>
                  <FilterItem onClick={() => this.handleFilterChange('Infográficos')} name="Infográficos" icon={<InfogIcon />} isActive={this.state.selectedFilter === 'Infográficos'} />
                  <FilterItem onClick={() => this.handleFilterChange('Webinars')} name="Webinars" icon={<WebinarIcon />} isActive={this.state.selectedFilter === 'Webinars'}/>
                  <FilterItem onClick={() => this.handleFilterChange('Ferramentas')} name="Ferramentas" icon={<ToolIcon />} isActive={this.state.selectedFilter === 'Ferramentas'} />
                  <FilterItem name="Materiais gratuitos :)" icon={null} />
                </div> 
              </Col>
            </Row>
          </Container>
        </div> 
      </>
  )}
}

export default Filter
