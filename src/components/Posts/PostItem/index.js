import React from "react"
import Img from "gatsby-image"
import { Link } from "gatsby"
import { Container, Row, Col } from 'react-grid-system';
import './styles.css';
import LinkIcon from '../../../icons/plus.svg';

const ResourcePost = ({ title, description, category, img, link, shouldShow } ) => {
  let categoryDisplay = category
  switch(category) {
    case 'E-books':
      categoryDisplay = 'o ebook';
      break;
    case 'Infográficos':
      categoryDisplay = 'o infográfico';
      break;
    case 'Webinar':
      categoryDisplay = 'o webinar';
      break;
    case 'Ferramentas':
      categoryDisplay = 'a ferramenta';
      break;
    default:
      categoryDisplay = category;
  }
  return(
   <Container className={`post-container ${shouldShow && 'visible'}`}>
     <Row>
       <Col>
         <div className='post-wrapper'>
           <Img
             fixed={img}
             imgStyle={{objectPosition: 'bottom'}}
           />
           <div className='post-card-wrapper'>
             <div className='post-card'>
                 <h3>{title}</h3>
                 <p>{description}</p>
                 <div className='post-link-wrap'>
                   <LinkIcon className='post-icon'/>
                   <Link className='post-link' to={link}>Saiba mais sobre {categoryDisplay} </Link>
                 </div>
               </div>
           </div>
         </div>
       </Col>
     </Row>
   </Container>
  )
}

export default ResourcePost
