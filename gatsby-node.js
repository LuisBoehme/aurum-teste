/**
 *
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
// You can delete this file if you're not using it
const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const ResourcePage = path.resolve("./src/pages/ResourcePage/index.js")
  const result = await graphql(`
    {
      allWordpressPost {
        edges {
          node {
            slug
            wordpress_id
          }
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  const ResourcePageList = result.data.allWordpressPost.edges
  ResourcePageList.forEach(post => {
    createPage({
      path: `/post/${post.node.slug}`,
      component: ResourcePage,
      context: {
        id: post.node.wordpress_id,
      },
    })
  })
}

